package com.semillero.tiendaComic.POO;

import java.math.BigDecimal;

import com.semillero.tiendaComic.enums.TipoVehiculoEnum;

public class Vehiculo {

	private String modelo;
	private String medioTransporte;
	private TipoVehiculoEnum tipoVehiculo;
	private String color;
	private double peso;
	private int numeroAsientos;
	private BigDecimal precio;
	
	public Vehiculo() {
	}

	public Vehiculo(String modelo, TipoVehiculoEnum tipoVehiculo, double peso, String color, int numeroAsientos, 
			BigDecimal precio) {
		this.modelo = modelo;
		this.tipoVehiculo = tipoVehiculo;
		this.peso = peso;
		this.color = color;
		this.numeroAsientos = numeroAsientos;
		this.precio = precio;
	}
	
	public Vehiculo(BigDecimal precio) {
		this.precio = precio;
	}
	
	/**
	 * @return the modelo
	 */
	public String getModelo() {
		return modelo;
	}
	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	/**
	 * @return the medioTransporte
	 */
	public String getMedioTransporte() {
		return medioTransporte;
	}
	/**
	 * @param medioTransporte the medioTransporte to set
	 */
	public void setMedioTransporte(String medioTransporte) {
		this.medioTransporte = medioTransporte;
	}
	
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the peso
	 */
	public double getPeso() {
		return peso;
	}
	/**
	 * @param peso the peso to set
	 */
	public void setPeso(double peso) {
		this.peso = peso;
	}
	/**
	 * @return the numeroAsientos
	 */
	public int getNumeroAsientos() {
		return numeroAsientos;
	}
	/**
	 * @param numeroAsientos the numeroAsientos to set
	 */
	public void setNumeroAsientos(int numeroAsientos) {
		this.numeroAsientos = numeroAsientos;
	}
	/**
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	/**
	 * @return the tipoVehiculo
	 */
	public TipoVehiculoEnum getTipoVehiculo() {
		return tipoVehiculo;
	}

	/**
	 * @param tipoVehiculo the tipoVehiculo to set
	 */
	public void setTipoVehiculo(TipoVehiculoEnum tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public int velocidad() {
		return 0;
	}
	
	public int velocidad(String valor, int presicion) {
		return 0;
	}
	
	public int velocidad(int valor, int presicion) {
		return 0;
	}
	
	@Override
	public String toString() {
		return "Vehiculo [modelo=" + modelo + ", medioTransporte=" + medioTransporte + ", tipoVehiculo=" + tipoVehiculo
				+ ", color=" + color + ", peso=" + peso + ", numeroAsientos=" + numeroAsientos + ", precio=" + precio
				+ "]";
	}
}
