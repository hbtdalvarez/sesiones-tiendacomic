package com.semillero.tiendaComic.POO;

import com.semillero.tiendaComic.abstracts.AccionesVehiculoAbstract;
import com.semillero.tiendaComic.enums.TipoVehiculoEnum;

public class Avion extends AccionesVehiculoAbstract {


	@Override
	public int obtenerVelocidadMaxima() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double obtenerPesoVehiculo() {
		return 100000000;
	}

	@Override
	public boolean determinarTipoVehiculo(TipoVehiculoEnum tipoVehiculoEnum) throws Exception {
		if(!TipoVehiculoEnum.AEREO.equals(tipoVehiculoEnum)) {
			throw new Exception("El vehiculo" + tipoVehiculoEnum.name() + "no es del tipo aereo");
		}
		System.out.println("El vehiculo es del mismo tipo");
		return true;
	}


}
