package com.semillero.tiendaComic.POO;

import java.math.BigDecimal;

import com.semillero.tiendaComic.enums.TipoVehiculoEnum;
import com.semillero.tiendaComic.interfaz.IAccionesVehiculoInterface;
import com.semillero.tiendaComic.interfaz.IOtraCosa;

public class Barco extends Vehiculo implements IAccionesVehiculoInterface, IOtraCosa {
	
	//primitivos int long short double float 
	//boolean chart
	
	//Wrappers u Objetos
	//String BigDecimal LocalDate LocaDateTime Number Boolean
	//Integer, Long, Short, Double, Float, Character
	private int numeroVelas;
	private String nombreCapitan;
	private String puertoLlegada;

	public Barco() {
		//Constructor vacio
		
	}
	
	public Barco(int numeroVelas) {
		this.numeroVelas = numeroVelas;
	}

	public Barco(int numeroVelas, String nombreCapitan, String puertoLlegada) {
		this.numeroVelas = numeroVelas;
		this.nombreCapitan = nombreCapitan;
		this.puertoLlegada = puertoLlegada;
	}
	
	public Barco(String modelo, TipoVehiculoEnum tipoVehiculo, double peso, String color, int numeroAsientos, 
			BigDecimal precio) {
		super(modelo,tipoVehiculo,peso,color,numeroAsientos,precio);
	}
	
	public Barco(BigDecimal precio) {
		super(precio);
//		super.setPrecio(precio);
	}
	
	/**
	 * @return the numeroVelas
	 */
	public int getNumeroVelas() {
		return numeroVelas;
	}

	/**
	 * @param numeroVelas the numeroVelas to set
	 */
	public void setNumeroVelas(int numeroVelas) {
		this.numeroVelas = numeroVelas;
	}

	/**
	 * @return the nombreCapitan
	 */
	public String getNombreCapitan() {
		return nombreCapitan;
	}

	/**
	 * @param nombreCapitan the nombreCapitan to set
	 */
	public void setNombreCapitan(String nombreCapitan) {
		this.nombreCapitan = nombreCapitan;
	}

	/**
	 * @return the puertoLlegada
	 */
	public String getPuertoLlegada() {
		return puertoLlegada;
	}

	/**
	 * @param puertoLlegada the puertoLlegada to set
	 */
	public void setPuertoLlegada(String puertoLlegada) {
		this.puertoLlegada = puertoLlegada;
	}
	
	public int obtenerVelocidad() {
		this.numeroVelas = 2;
		int velocidad = 100;
		//int a = aceleracion + velocidad;
		if(velocidad > 0) {
			int aceleracion = 50;
			int a = aceleracion + velocidad;
		}
		return velocidad;
	}

	@Override
	public String toString() {
		String valores = super.toString();
		return "Barco [numeroVelas=" + numeroVelas + ", nombreCapitan=" + nombreCapitan + ", puertoLlegada="
				+ puertoLlegada + "]" + " " + valores;
	}

	@Override
	public int obtenerVelocidadMaxima() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double obtenerPesoVehiculo() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean determinarTipoVehiculo(TipoVehiculoEnum tipoVehiculoEnum) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
}
