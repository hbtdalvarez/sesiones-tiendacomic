package com.semillero.tiendaComic.abstracts;

import com.semillero.tiendaComic.enums.TipoVehiculoEnum;

public abstract class AccionesVehiculoAbstract {

	public abstract int obtenerVelocidadMaxima();
	
	public abstract double obtenerPesoVehiculo();
	
	public abstract boolean determinarTipoVehiculo(TipoVehiculoEnum tipoVehiculoEnum) throws Exception;
	
	public Long velocidad() {
		return 100L;
	}
}
