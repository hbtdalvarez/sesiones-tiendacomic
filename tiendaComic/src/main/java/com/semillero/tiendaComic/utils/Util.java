package com.semillero.tiendaComic.utils;


import org.springframework.http.HttpStatus;

import com.semillero.tiendaComic.dto.JSONResponse;
import com.semillero.tiendaComic.enums.JSONResponseStatus;

public abstract class Util {
	
	/**
     * @param <T>
     * @param data
     * @return <T> ApiResponseEntity<T>
     */
    protected <T> JSONResponse<T> buildResponse (T data) {
        return new JSONResponse<>(JSONResponseStatus.SUCCESS.toString(), HttpStatus.OK, data);
    }

    /**
     * @param <T>
     * @param data
     * @param message
     * @param httpStatus
     * @return <T> ApiResponseEntity<T>
     */
    protected <T> JSONResponse<T> buildErrorResponse (T error, HttpStatus httpStatus) {
        return new JSONResponse<>(JSONResponseStatus.ERROR.toString(), httpStatus, error);
    }
}
