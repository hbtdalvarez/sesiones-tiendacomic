package com.semillero.tiendaComic.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.semillero.tiendaComic.controllers.GestionarComicController;
import com.semillero.tiendaComic.dto.ConsultaNombrePrecioComicDTO;
import com.semillero.tiendaComic.enums.TICOEnum;
import com.semillero.tiendaComic.exception.GestionarComicException;
import com.semillero.tiendaComic.exception.TiendaComicException;
import com.semillero.tiendaComic.interfaz.ComicDTO;
import com.semillero.tiendaComic.interfaz.IGestionarComic;
import com.semillero.tiendaComic.model.Comic;
import com.semillero.tiendaComic.repository.ComicRepository;

@Service
public class GestionarComicService implements IGestionarComic {
	
	private final static Logger LOG = LogManager.getLogger(GestionarComicService.class);
	
	@Autowired
	private ComicRepository comicRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public void createComic(ComicDTO comicDTo) throws TiendaComicException, GestionarComicException {
		LOG.info("Inicia metodo createComic() con data {}", comicDTo.toString());
		if(comicDTo != null && comicDTo.getNombre().length() > 50) {
			LOG.error("Se ha presentado excepcion GestionarComicException con mensaje {}", TICOEnum.TICO_0005.getMessage());
			throw new GestionarComicException(TICOEnum.TICO_0005);
		}
		
		Comic comic = modelMapper.map(comicDTo, Comic.class);
		comicRepository.save(comic);
		LOG.info("Finaliza metodo createComic() con id {}", comic.getId());
	}

	@Override
	public ComicDTO getComic(Long idComic) throws TiendaComicException {
		Optional<Comic> comic = comicRepository.findById(idComic);
		ComicDTO comicDTO = null;
		if(comic.isPresent()) {
			comicDTO = modelMapper.map(comic, ComicDTO.class);
		}
		return comicDTO;
	}

	@Override
	public void deleteComic(Long idComic) throws TiendaComicException {
		comicRepository.deleteById(idComic);
	}

	@Override
	public List<ComicDTO> getComics() throws TiendaComicException {
		List<ComicDTO> comicsDto = new ArrayList<>();
		List<Comic> comics = comicRepository.findAll();
		for (Comic comic : comics) {
			ComicDTO comicdto = modelMapper.map(comic, ComicDTO.class);
			comicsDto.add(comicdto);
		}
		return comicsDto;
	}

	@Override
	public ConsultaNombrePrecioComicDTO consultarNombrePrecioComic(Long idComic) throws GestionarComicException, TiendaComicException {
		if(idComic <= 0) {
			throw new GestionarComicException(TICOEnum.TICO_0001, idComic.toString(), "1", "5", "8");	
		}
		ConsultaNombrePrecioComicDTO dto = comicRepository.obtenerNombreYPrecio(idComic);
		return dto;
		
		//return buildErrorResponse(new ErrorResponse(TICOEnum.TICO_9999.getCode(),TICOEnum.TICO_9999.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
