package com.semillero.tiendaComic.interfaz;

import com.semillero.tiendaComic.enums.TipoVehiculoEnum;

public interface IAccionesVehiculoInterface {
	
	public int obtenerVelocidadMaxima();
	
	public double obtenerPesoVehiculo();
	
	public boolean determinarTipoVehiculo(TipoVehiculoEnum tipoVehiculoEnum) throws Exception;
	
//	public default Long velocidad() {
//		return 100L;
//	}
}
