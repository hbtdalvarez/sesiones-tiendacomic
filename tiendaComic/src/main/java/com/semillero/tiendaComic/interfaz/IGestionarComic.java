package com.semillero.tiendaComic.interfaz;

import java.util.List;

import com.semillero.tiendaComic.dto.ConsultaNombrePrecioComicDTO;
import com.semillero.tiendaComic.exception.GestionarComicException;
import com.semillero.tiendaComic.exception.TiendaComicException;

public interface IGestionarComic {

	public void createComic(ComicDTO comicDTo) throws TiendaComicException, GestionarComicException;

	public ComicDTO getComic(Long idComic) throws TiendaComicException;
	
	public void deleteComic(Long idComic) throws TiendaComicException;
	
	public List<ComicDTO> getComics() throws TiendaComicException;
	
	public ConsultaNombrePrecioComicDTO consultarNombrePrecioComic(Long idComic) throws TiendaComicException, GestionarComicException;
	
}
