package com.semillero.tiendaComic.interfaz;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import com.semillero.tiendaComic.enums.EstadoEnum;
import com.semillero.tiendaComic.enums.TematicaEnum;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <b>Descripción:<b> Clase que determina el dto a usar para modificar,
 * consultar y posteriormente eliminar un comic
 * 
 * @author ccastano
 */
@Getter
@Setter
@ToString
public class ComicDTO implements Serializable {

	/**
	 * Atributo que determina
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String nombre;
	private String editorial;
	private TematicaEnum tematicaEnum;
	private String coleccion;
	private Integer numeroPaginas;
	private BigDecimal precio;
	private String autores;
	private Boolean color;
	private LocalDate fechaVenta;
	private EstadoEnum estadoEnum;
	private Long cantidad;

	public ComicDTO() {
		//Contructor vacio
	}
}
