package com.semillero.tiendaComic.controllers;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.semillero.tiendaComic.exception.GestionarComicException;
import com.semillero.tiendaComic.exception.TiendaComicException;
import com.semillero.tiendaComic.interfaz.ComicDTO;
import com.semillero.tiendaComic.interfaz.IGestionarComic;
import com.semillero.tiendaComic.utils.Util;

@RestController
@RequestMapping("tiendaComic")
@CrossOrigin(origins = "http://localhost:4200")
public class GestionarComicController extends Util {
	
	private final static Logger LOG = LogManager.getLogger(GestionarComicController.class);
	
	@Autowired
	private IGestionarComic gestionarComicService;
	
	//localhost:9091/tiendaComic/createComic
	@PostMapping("createComic")
	public ResponseEntity<?> createComic(@RequestBody ComicDTO comicDTO) throws TiendaComicException, GestionarComicException {
		LOG.info("Inicia createComic() con data {}", comicDTO.toString());
		
		gestionarComicService.createComic(comicDTO);
		String response = "Se ha creado el comic exitosamente";
		
		LOG.info("Finaliza createComic() con response {}", response);
		return buildResponse(response);
	}
	
	@GetMapping("obtenerComic")
	public ResponseEntity<?> obtenerComic(@RequestParam Long idComic) throws TiendaComicException {
		return buildResponse(gestionarComicService.getComic(idComic));
	}
	
	@GetMapping("obtenerComics")
	public ResponseEntity<?> obtenerComics() throws TiendaComicException {
		return buildResponse(gestionarComicService.getComics());
	}
	
	@DeleteMapping("deleteComic/{idComic}")
	public ResponseEntity<?> deleteComic(@PathVariable(name = "idComic") Long idComic) throws TiendaComicException {
		gestionarComicService.deleteComic(idComic);
		String response = "El comic " + idComic + " ha sido eliminado";
		return buildResponse(response);
	}
	
	@GetMapping("consultarNombrePrecioComic")
	public ResponseEntity<?> consultarNombrePrecioComic(@RequestParam Long idComic) throws TiendaComicException, GestionarComicException {
		return buildResponse(gestionarComicService.consultarNombrePrecioComic(idComic));
	}
}
