package com.semillero.tiendaComic.controllers;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.semillero.tiendaComic.dto.ConsultaNombrePrecioComicDTO;
import com.semillero.tiendaComic.enums.EstadoEnum;
import com.semillero.tiendaComic.model.Comic;
import com.semillero.tiendaComic.repository.ComicRepository;

/**
 * 
 * @author DIEGO FERNANDO
 *
 */
@RestController
@RequestMapping("test")
public class SpringDataController {
	
	@Autowired
	private ComicRepository comicRepository;

	@GetMapping()
	public String test() {
//		if(comic != null) {
//			
//		}
//		if(comics.size() > 20)
//			comics.get(10);
//		Optional<Comic> comic = null;
//		Comic comic2 = null;
//		List<Comic> comics = null;
		try {
//			
//			Long cantidadRegistros = comicRepository.count();
//			Boolean existeRegistro = comicRepository.existsById(1L);
//			comic2 = comicRepository.getOne(12L);
//			if(comic2 != null) {
//				
//			}
//			comic = comicRepository.findById(15L);
//			if(comic.isPresent()) {
//				Long id = comic.get().getId();
//				String nombre = comic.get().getNombre();
//			}
//			
//			comics = comicRepository.findAll();
			//Genera exception 
			//comic2 = comicRepository.findById(1L).get();

			//Creacion registro en base de datos de la entidad Comic
			Comic comicSuperman = new Comic(); 
//			comicSuperman.setNombre("Chavo del 8");
//			comicSuperman.setEditorial("DC");
//			comicSuperman.setColeccion("DC");
//			comicSuperman.setNumeroPaginas(100);
////			BigDecimal precio = new BigDecimal(1000);
//			comicSuperman.setPrecio(new BigDecimal(1000));
//			comicSuperman.setCantidad(10L);
			
//			comicSuperman = comicRepository.save(comicSuperman);
			
//			comicSuperman.setId(15L);
//			comicSuperman.setTematicaEnum(TematicaEnum.HORROR);
//			comicSuperman.setNombre("Spaw");
//			comicSuperman.setEditorial("DC");
//			comicSuperman.setColeccion("DC");
//			comicSuperman.setNumeroPaginas(100);
//			comicSuperman.setPrecio(new BigDecimal(1000));
//			comicSuperman.setCantidad(10L);
//			
//			comicSuperman = comicRepository.save(comicSuperman);
//			
//			comicRepository.saveAll(comics);
			
			List<Long> idsComics = new ArrayList<>();
//			idsComics.add(1L);
//			idsComics.add(2L);
//			idsComics.add(3L);
//			idsComics.add(4L);
//			comicRepository.deleteAllById(idsComics);
//			
//			comicRepository.deleteById(12L);
//			comic = comicRepository.findById(12L);
			
			List<String> nombresComics = comicRepository.findAllNamesComics();
			Comic comic = null;
			List<Comic> listaComics = null;
			comic = comicRepository.obtenerComicPorId(16L);
			
			
//			listaComics = comicRepository.obtenerComicPorCaracterYEstadoEnum("a", EstadoEnum.ACTIVO);
//			
//			comic = comicRepository.consultarComicPorNombre("Chavo del 8");
//			
//			
//			int actualizados = comicRepository.actualizarEstadoComicPorId(EstadoEnum.ACTIVO, 13L);
//			
//			List<Long> idsComics2 = new ArrayList<>();
//			idsComics2.add(15L);
//			idsComics2.add(14L);
//			int contadorActualizados = comicRepository.actualizarEstadoIdsComic(EstadoEnum.ACTIVO, idsComics2);
//			
//			
//			int registrosActualizados = comicRepository.eliminarComicPorEstado(EstadoEnum.INACTIVO);
//			
			Object[][] data = comicRepository.obtenerComicNative(20L);
			String nombre = (String) data[0][0];
			BigDecimal precio = (BigDecimal) data[0][1];
			
			ConsultaNombrePrecioComicDTO dataDto = comicRepository.obtenerNombreYPrecio(24L);
			nombre = dataDto.getNombre();
			precio = dataDto.getPrecio();
			//20,13,14
//			PageRequest page = PageRequest.of(1, 1);
//			Page<String> listaPaginada = comicRepository.findAllNamesComics(page);
			
			listaComics = comicRepository.findByNombre("Spaw");
			listaComics = comicRepository.findByNombreIsNot("Spaw");
			
			listaComics = comicRepository.findByFechaVentaIsNull();
			listaComics = comicRepository.findByFechaVentaIsNotNull();
			
			listaComics = comicRepository.findByColorTrue();
			listaComics = comicRepository.findByColorFalse();
			
			listaComics = comicRepository.findByNombreContaining("aw");
			
			listaComics = comicRepository.findByPrecioLessThan(new BigDecimal(5000));
			listaComics = comicRepository.findByPrecioLessThanEqual(new BigDecimal(6000));
			
			listaComics = comicRepository.findByPrecioGreaterThan(new BigDecimal(5000));
			listaComics = comicRepository.findByPrecioGreaterThanEqual(new BigDecimal(6000));

			listaComics = comicRepository.findByPrecioGreaterThanEqualAndEstadoEnumEquals(new BigDecimal(5000), EstadoEnum.ACTIVO);
			
			System.out.println("");
		} catch (Exception e) {
			System.out.println(e);
		}
		
	
		return null;
	}
}
