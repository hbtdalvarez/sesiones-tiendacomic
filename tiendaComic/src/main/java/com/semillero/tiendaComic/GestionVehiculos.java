package com.semillero.tiendaComic;

import com.semillero.tiendaComic.POO.Avion;
import com.semillero.tiendaComic.POO.Barco;
import com.semillero.tiendaComic.POO.Bicicleta;
import com.semillero.tiendaComic.POO.Vehiculo;
import com.semillero.tiendaComic.enums.TipoVehiculoEnum;

public class GestionVehiculos {
	//private static final String ACUATICO = "ACUATICO";
	public static void main(String[] args) {
		int numeroVelas = 2;
		String nombreCapitan = "Ricardo";
		String nombrePuerto = "Puerto 10";
		
		Barco barco0 = new Barco();
		barco0.setNumeroVelas(2);
		barco0.setPuertoLlegada("Puerto 1");
		Barco barco1 = new Barco(numeroVelas);
		Barco barco2 = new Barco(numeroVelas, nombreCapitan, nombrePuerto);
		
		System.out.println("Valor del Barco1 = " + barco1.toString());
		System.out.println("Valor del Barco2 = " + barco2.toString());
		System.out.println("numero velas Barco0 = " + barco0.getNumeroVelas() + 
				"Velocidad barco: " + barco0.obtenerVelocidad()+"Km");
		
		Barco barco5 = new Barco();
		barco5.setColor("Blanco");
		barco5.setTipoVehiculo(TipoVehiculoEnum.ACUATICO);
		
		Vehiculo submarino = new Barco();
		//submarino.set
		
		System.out.println(barco5.toString());
//		if(barco5.getTipoVehiculo().getId() != TipoVehiculoEnum.ACUATICO.getId()) {
//			
//		}
		
		Avion avion1 = new Avion();
		System.out.println("El peso del avion 1 es: " + avion1.obtenerPesoVehiculo());
		System.out.println("velocidad del avion 1 es: " + avion1.obtenerVelocidadMaxima());
		
		try {
			System.out.println("el tipo de avion " + avion1.determinarTipoVehiculo(TipoVehiculoEnum.AEREO));
		} catch (Exception e) {
			System.out.println("Se ha presentado un error:" + e.getMessage());
		}
		
		
		Bicicleta bicicleta = new Bicicleta();
		System.out.println("Peso bicicleta: " + bicicleta.obtenerPesoVehiculo() );
		System.out.println("Velocidad Bici : " + bicicleta.obtenerVelocidadMaxima());
	}
}

