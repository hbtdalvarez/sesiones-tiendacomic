package com.semillero.tiendaComic.repository;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.semillero.tiendaComic.dto.ConsultaNombrePrecioComicDTO;
import com.semillero.tiendaComic.enums.EstadoEnum;
import com.semillero.tiendaComic.exception.GestionarComicException;
import com.semillero.tiendaComic.model.Comic;

@Repository
public interface ComicRepository extends JpaRepository<Comic, Long> {

	List<Comic> findByNombre(String nombre);
	List<Comic> findByNombreIsNot(String nombre);
	
	List<Comic> findByFechaVentaIsNull();
	List<Comic> findByFechaVentaIsNotNull();
	
	List<Comic> findByColorTrue();
	List<Comic> findByColorFalse();
	
	List<Comic> findByNombreContaining(String cadena);
	
	List<Comic> findByPrecioLessThan(BigDecimal precio);
	List<Comic> findByPrecioLessThanEqual(BigDecimal precio);
	
	List<Comic> findByPrecioGreaterThan(BigDecimal precio);
	List<Comic> findByPrecioGreaterThanEqual(BigDecimal precio);

	List<Comic> findByPrecioGreaterThanEqualAndEstadoEnumEquals(BigDecimal precio, EstadoEnum estado);
	
	
	//SELECT c.SCNOMBRE FROM COMIC c
	@Query("SELECT nombre FROM Comic ")
	List<String> findAllNamesComics();
	
	@Query("SELECT c.nombre FROM Comic c")
	Page<String> findAllNamesComics(PageRequest page);
	
	//SELECT * FROM COMIC WHERE SCID = 12
	@Query("SELECT c FROM Comic c WHERE c.id = :idComic ")
	Comic obtenerComicPorId(@Param("idComic") Long idComic);
	
	@Query("SELECT c FROM Comic c WHERE c.nombre LIKE (%:caracter%) AND c.estadoEnum = :estado ORDER BY c.nombre DESC")
	List<Comic> obtenerComicPorCaracterYEstadoEnum(@Param("caracter") String caracter, @Param("estado") EstadoEnum estado);
	
	@Query("SELECT c FROM Comic c WHERE c.nombre = :nombreComic")
	Comic consultarComicPorNombre(@Param("nombreComic") String nombreComic);
	
	@Query("UPDATE Comic c SET c.estadoEnum = :estado WHERE c.id = :idComic")
	@Modifying
	@Transactional
	int actualizarEstadoComicPorId(@Param("estado") EstadoEnum estado, @Param("idComic") Long idComic);
	
	@Query("UPDATE Comic c SET c.estadoEnum = :estado WHERE c.id IN (:listIdComics)")
	@Modifying
	@Transactional
	int actualizarEstadoIdsComic(@Param("estado") EstadoEnum estado, @Param("listIdComics") List<Long> idComic);
	
	@Query("DELETE FROM Comic WHERE estadoEnum = :estado")
	@Modifying
	@Transactional
	int eliminarComicPorEstado(@Param("estado") EstadoEnum estadoEnum);
	
//	@Query("INSERT INTO Comic(nombre,editorial) VALUES (:nombre,:editorial)")
//	@Modifying
//	@Transactional
//	int crearComicInsert(@Param("estado") EstadoEnum estadoEnum);
	
	@Query(value = "SELECT SCNOMBRE, SCPRECIO, SCESTADO FROM COMIC WHERE SCID = :idComic", nativeQuery = true)
	public Object[][] obtenerComicNative(@Param("idComic") Long idComic);

	@Query("SELECT new com.semillero.tiendaComic.dto.ConsultaNombrePrecioComicDTO(c.nombre, c.precio) FROM Comic c WHERE c.id = :idComic")
	ConsultaNombrePrecioComicDTO obtenerNombreYPrecio(@Param("idComic") Long idComic) throws GestionarComicException;
}
