//package com.semillero.tiendaComic.model;
//
//import java.util.List;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToMany;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "COMPRA_COMIC")
//public class CompraComic {
//
//	@Id
//	@SequenceGenerator(name = "COMPRACOMIC_SCCID_GENERATOR", sequenceName = "SEQ_COMPRA_COMIC", allocationSize = 1)
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMPRACOMIC_SCCID_GENERATOR")
//	@Column(name="SUID")
//	private Long id;
//
//	@JoinColumn(name="compraComic")
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	private List<Comic> compraComic;
//}
