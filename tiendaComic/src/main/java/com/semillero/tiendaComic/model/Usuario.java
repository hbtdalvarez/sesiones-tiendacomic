//package com.semillero.tiendaComic.model;
//
//import java.util.List;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToMany;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "USUARIO")
//public class Usuario {
//
//	@Id
//	@SequenceGenerator(name = "USUARIO_SUID_GENERATOR", sequenceName = "SEQ_USUARIO", allocationSize = 1)
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USUARIO_SUID_GENERATOR")
//	@Column(name="SUID")
//	private Long id;
//	
//	@Column(name="SUNOMBRE")
//	private String nombre;
//	
//	@JoinColumn(name="UsuarioCompra")
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	private List<CompraComic> compraComic;
//	
//
//	/**
//	 * @return the id
//	 */
//	public Long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the nombre
//	 */
//	public String getNombre() {
//		return nombre;
//	}
//
//	/**
//	 * @param nombre the nombre to set
//	 */
//	public void setNombre(String nombre) {
//		this.nombre = nombre;
//	}
//
//	/**
//	 * @return the compraComic
//	 */
//	public List<CompraComic> getCompraComic() {
//		return compraComic;
//	}
//
//	/**
//	 * @param compraComic the compraComic to set
//	 */
//	public void setCompraComic(List<CompraComic> compraComic) {
//		this.compraComic = compraComic;
//	}
//	
//	
//}
