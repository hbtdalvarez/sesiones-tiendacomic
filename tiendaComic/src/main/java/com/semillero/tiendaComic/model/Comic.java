package com.semillero.tiendaComic.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.semillero.tiendaComic.enums.EstadoEnum;
import com.semillero.tiendaComic.enums.TematicaEnum;

@Entity
@Table(name = "COMIC")
public class Comic {
	
	@Id
	@SequenceGenerator(name = "COMIC_SCID_GENERATOR", sequenceName = "SEQ_COMIC", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMIC_SCID_GENERATOR")
	@Column(name="SCID")
	private Long id;
	
	@Column(name="SCNOMBRE")
	private String nombre;
	
	@Column(name="SCEDITORIAL")
	private String editorial;
	
	@Column(name="SCTEMATICA")
	@Enumerated(value = EnumType.STRING)
	private TematicaEnum tematicaEnum;
	
	@Column(name = "SCCOLECCION")
	private String coleccion;
	
	@Column(name = "SCNUMEROPAGINAS")
	private Integer numeroPaginas;
	
	@Column(name = "SCAUTORES")
	private String autores;
	
	@Column(name="SCPRECIO")
	private BigDecimal precio;
	//LocalDateTime
	//LocalDate
	//LocalTime
	//Period
	@Column(name="SCFECHA_VENTA")
	private LocalDate fechaVenta;
	
	@Column(name="SCCOLOR")
	private Boolean color;

	@Column(name = "SCESTADO")
	@Enumerated(value = EnumType.STRING)
	private EstadoEnum estadoEnum;

	@Column(name = "SCCANTIDAD")
	private Long cantidad;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the editorial
	 */
	public String getEditorial() {
		return editorial;
	}

	/**
	 * @param editorial the editorial to set
	 */
	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	/**
	 * @return the tematicaEnum
	 */
	public TematicaEnum getTematicaEnum() {
		return tematicaEnum;
	}

	/**
	 * @param tematicaEnum the tematicaEnum to set
	 */
	public void setTematicaEnum(TematicaEnum tematicaEnum) {
		this.tematicaEnum = tematicaEnum;
	}

	/**
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	/**
	 * @return the fechaVenta
	 */
	public LocalDate getFechaVenta() {
		return fechaVenta;
	}

	/**
	 * @param fechaVenta the fechaVenta to set
	 */
	public void setFechaVenta(LocalDate fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	/**
	 * @return the color
	 */
	public Boolean getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(Boolean color) {
		this.color = color;
	}

	/**
	 * @return the coleccion
	 */
	public String getColeccion() {
		return coleccion;
	}

	/**
	 * @param coleccion the coleccion to set
	 */
	public void setColeccion(String coleccion) {
		this.coleccion = coleccion;
	}

	/**
	 * @return the numeroPaginas
	 */
	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}

	/**
	 * @param numeroPaginas the numeroPaginas to set
	 */
	public void setNumeroPaginas(Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	/**
	 * @return the autores
	 */
	public String getAutores() {
		return autores;
	}

	/**
	 * @param autores the autores to set
	 */
	public void setAutores(String autores) {
		this.autores = autores;
	}

	/**
	 * @return the estadoEnum
	 */
	public EstadoEnum getEstadoEnum() {
		return estadoEnum;
	}

	/**
	 * @param estadoEnum the estadoEnum to set
	 */
	public void setEstadoEnum(EstadoEnum estadoEnum) {
		this.estadoEnum = estadoEnum;
	}

	/**
	 * @return the cantidad
	 */
	public Long getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}
}
