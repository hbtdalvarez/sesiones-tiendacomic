package com.semillero.tiendaComic.exception;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.semillero.tiendaComic.dto.ErrorResponse;
import com.semillero.tiendaComic.dto.JSONResponse;
import com.semillero.tiendaComic.enums.JSONResponseStatus;
import com.semillero.tiendaComic.enums.TICOEnum;





/**
 * 
 * @author erik.alvarez
 * @since Java 1.8
 * @version 1.0
 * @see ExceptionResponseHandler
 */
@ControllerAdvice
public class ExceptionResponseHandler extends ResponseEntityExceptionHandler {

	private final static Logger LOG = LogManager.getLogger(ExceptionResponseHandler.class);
		
	
    /**
     * 
     * @param ex
     * @param request
     * @return ApiResponseEntity<String>
     */
    @ExceptionHandler({ Exception.class })
    public JSONResponse<Object> handleGeneralException (Exception ex) {
    	LOG.error("General Exception: message: {}, statusCode: {}", ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
    	return new JSONResponse<>(JSONResponseStatus.ERROR.toString(), HttpStatus.INTERNAL_SERVER_ERROR, 
    			new ErrorResponse(TICOEnum.TICO_9999.getMessage(), TICOEnum.TICO_9999.name()));
    }
    
    @ExceptionHandler({ GestionarComicException.class })
    public JSONResponse<Object> handlerGestionarComicException (GestionarComicException ex) {
    	LOG.error("Inicia GestionarComicException: "+ ex);
    	
    	String message = ex.getMessage();
		String errors = String.format(message, ex.getParams());
		
		LOG.error("Finaliza GestionarComicException: code: {}", HttpStatus.CONFLICT);
    	return new JSONResponse<>(JSONResponseStatus.ERROR.toString(), HttpStatus.CONFLICT, 
    			new ErrorResponse(errors, ex.getCode()));
    }
    
}
