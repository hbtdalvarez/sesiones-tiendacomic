package com.semillero.tiendaComic.dto;

import java.math.BigDecimal;

public class ConsultaNombrePrecioComicDTO {

	private String nombre;
	private BigDecimal precio;
	
	public ConsultaNombrePrecioComicDTO( ) {
		//Constructor vacio
	}
	
	public ConsultaNombrePrecioComicDTO(String nombre, BigDecimal precio) {
		this.nombre = nombre;
		this.precio = precio;
	}
	
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}
}
