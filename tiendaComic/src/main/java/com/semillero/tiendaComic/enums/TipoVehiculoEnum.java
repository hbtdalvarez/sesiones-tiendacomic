package com.semillero.tiendaComic.enums;

public enum TipoVehiculoEnum {

	TERRESTRE(1,"Terrestre"),
	ACUATICO(2,"Acuatico"),
	AEREO(3,"Aereo"),
	ESPACIAL(4,"Espacial")	
	
	;
	
	private int id;
	private String valor;
	
	TipoVehiculoEnum(int id, String valor) {
		this.id = id;
		this.valor = valor;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}
}
