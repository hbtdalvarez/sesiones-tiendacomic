package com.semillero.tiendaComic.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.semillero.tiendaComic.exception.GestionarComicException;
import com.semillero.tiendaComic.exception.TiendaComicException;
import com.semillero.tiendaComic.interfaz.ComicDTO;
import com.semillero.tiendaComic.interfaz.IGestionarComic;

public class GestionarComicControllerTest {

	@InjectMocks
	private GestionarComicController gestionarComicController;
	@Mock
	private IGestionarComic gestionarComicService;
	
	@BeforeEach
	public void setup() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	public void whenCreateComicGivenNewComicThenResponseSuccess() throws GestionarComicException, TiendaComicException {
		//any()
		ResponseEntity<?> response = gestionarComicController.createComic(new ComicDTO());
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
}
