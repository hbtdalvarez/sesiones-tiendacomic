package com.semillero.tiendaComic.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import com.semillero.tiendaComic.dto.ConsultaNombrePrecioComicDTO;
import com.semillero.tiendaComic.exception.GestionarComicException;
import com.semillero.tiendaComic.exception.TiendaComicException;
import com.semillero.tiendaComic.repository.ComicRepository;
import com.semillero.tiendaComic.services.GestionarComicService;

public class GestionarComicServiceTest {

	@InjectMocks
	private GestionarComicService gestionarComicService;
	@Mock
	private ComicRepository comicRepository;
	@Mock
	private ModelMapper mapper;
	
	@BeforeEach
	public void setup() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	public void whenConsultarNombrePrecioComic_givenGestionarComicException_thenError() throws GestionarComicException {
		//doThrow(GestionarComicException.class).when()
		GestionarComicException exception = assertThrows(GestionarComicException.class, 
				()-> gestionarComicService.consultarNombrePrecioComic(-1L) );
		assertNotNull(exception);
		assertTrue(exception.getMessage().contains("debe ser mayor a 0"));
		//assertEquals(exception.getMessage(), "El idComic -1 1 5 8 debe ser mayor a 0");
	}
	
	@Test
	public void whenConsultarNombrePrecioComic_givenGestionarComicException_thenFail() throws GestionarComicException {
		doThrow(GestionarComicException.class).when(comicRepository).obtenerNombreYPrecio(any());
		GestionarComicException exception = assertThrows(GestionarComicException.class, () -> gestionarComicService.consultarNombrePrecioComic(0L));
		assertNotNull(exception);
		assertTrue(exception.getMessage().contains("El idComic "));
	}
	
	@Test
	public void whenConsultarNombrePrecioComic_givenComicWithNameAndPriceThenResponseSucess() throws GestionarComicException, TiendaComicException {
		ConsultaNombrePrecioComicDTO dto = new ConsultaNombrePrecioComicDTO();
		dto.setNombre("Spaw");
		dto.setPrecio(new BigDecimal(1000));
		when(comicRepository.obtenerNombreYPrecio(any())).thenReturn(dto);
		ConsultaNombrePrecioComicDTO result =gestionarComicService
				.consultarNombrePrecioComic(11L);
		assertNotNull(result);
		assertEquals(result.getNombre(), "Spaw");
		assertEquals(result.getPrecio(), new BigDecimal(1000));
	}
}
