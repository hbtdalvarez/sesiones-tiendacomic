package com.semillero.tiendaComic;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TiendaComicApplicationTests {

	@InjectMocks
	GestionVehiculos gestionarVehiculos;
	
	@BeforeEach
	public void setup( ) {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	void contextLoads() {
	}

}
